# MindBreak

Software Define System - Final Project

How to set up Kubernetes cluster
1.	2 Master Nodes 1 Loadbalancer Spec.
-	Ubuntu 16.04 VM
-	2 CPU Cores
-	Ram : 4 GB
2.	4 Worker Nodes Spec.
-	Raspberry pi 3 B+ with operating system Raspbain Strench Lite
-	Create empty file �ssh� and store in Raspberry pi
3.	Set Static IP Address in 2 Master Nodes, 4 Worker Nodes and Loadbalancer
-	Worker Node
o	192.168.0.151
o	192.168.0.152
o	192.168.0.153
o	192.168.0.154
-	Master Node
o	192.168.0.90
o	192.168.0.91
-	Loadbalancer
o	192.168.0.93
4.	Connect computer1 which have Master Node and Lordbalancer and computer2 which have only Master Node with TP-Link router though wireless connection and connect raspberry pi with TP-Link router though Ethernet cable
5.	Worker node configuration
-	Install docker and set its permission
$ curl -sSL get.docker.com | sh && \
sudo usermod pi -aG docker && \
newgrp dockerDisble swap
-	Disable swap
$ sudo dphys-swapfile swapoff && \
sudo dphys-swapfile uninstall && \
sudo update-rc.d dphys-swapfile remove
-	Edit the /boot/cmdline.txt file. Add the following in the end of the file
$ cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
-	Reboot
$ Sudo reboot
-	Edit the /etc/apt/sources.list.d/kubernetes.list by add following in the file
deb http://apt.kubernetes.io/ kubernetes-xenial main
-	Add the key
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
-	Update with new repo
$ sudo apt-get update
-	Install kubeadm
$ sudo apt-get install �qy kubeadm
6.	Master node and loadbalancer configuration
-	Installing cfssl on loadbalancer
o	Download the binaries
$ wget https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
$ wget https://pkg/cfssl.org/R1.2/cfssljson_linx-amd64
o	Add the execution permission to the binaries
$ chmod +x cfssl*
o	Move the binaries to /usr/local/bin.
$ sudo mv cfssl_linux-amd64 /usr/local/bin/cfssl
$ sudo mv cfssljson_linux-amd64 /usr/local/bin/cfssljson
-	Installing kubectl on loadbalancer
o	Download the binary
$ wget https://storage.googleapis.com/kubernetes-release/release/v1.12.1/bin/linux/amd64/kubectl
o	 Add the execution permission to the binary
$ chmod +x kubectl
o	Move the binary to /usr/local/bin
$ sudo mv kubectl /usr/local/bin
-	Installing the HAProxy load balancer
o	Update the machine
$ sudo apt-get update
$ sudo apt-get upgrade
o	Install HAProxy
$ sudo apt-get install haproxy
o	Configure HAProxy to load  balance the traffic between the two Kubernetes master nodes
$ sudo nano /etc/haproxy/haproxy.cfg
global
�
Default
�
frontend kubernetes
bind 192.168.0.93:6443
option tcplog
mode tcp
default_backend kubernetes-master-nodes

backend kubernetes-master-nodes
mode tcp
balance roundrobin
option tcp-check
server k8s-master-0 192.168.0.90 check fall 2 rise 1
server k8s-master-1 192.168.0.91 check fall 2 rise 1
o	Restart HAProxy
$ sudo systemctl restart haproxy
-	Creating A certificate authority
o	Create the certificate authority configuration file
$ nano ca-config.json
{
	�signing�: {
		�default�: {
			�expiry�: �8760h�
},
�profiles�: {
	�kubernetes�: {
		�usages�: [�signing�, �key encipherment�, �server auth�, �client auth�],
		�expiry�: �8760h�
}
}
}
}
o	Create the certificate authority signing request configuration file
$ nano ca-csr.json
{
	�CN�: �Kubernates�,
	�key�: {
		�algo�: �rsa�,
		�size�: 2048
},
�names�:[
{
		�C�: �IE�,
		�L�: �Cork�,
		�O�: �Kubernetes�,
		�OU�: �CA�,
		�ST�: �Cork Co.�
}
]
}
o	Generate the certificate authority certificate and private key
$ cfssl gencert �initca ca-csr.json | cfssljson �bare ca
-	Creating the certificate for the Etcd cluster
o	Create the certificate signing request configuration file
$ nano kubernetes-csr.json
{
	�CN�: �kubernetes�,
	�key�: {
		�algo�: �rsa�,
		�size�: 2048
},
�names�: [
	{
		�C�: �IE�,
		�L�: �Cork�,
		�O�: �Kubernetes�,
		�OU�: �Kubernetes�,
		�ST�: �Cork Co.�
}
]
}
o	Generate the certificate and private key
$ cfssl gencert \
- ca=ca.pem \
- ca-key=ca-key.pem \
- config=ca-config.json \
-hostname=192.168.0.90,192.168.0.91,192.168.0.93,127.0.0.1,kubernetes.default \
- profile=kubernetes kubernetes-csr.json | \
cfssljson �bare kubernetes
o	Copy the certificate to each nodes
scp ca.pem kubernetes.pem kubernetes-key.pem master1@192.168.0.90:~
scp ca.pem kubernetes.pem kubernetes-key.pem ppp@192.168.0.91:~
scp ca.pem kubernetes.pem kubernetes-key.pem loadBalancer@192.168.0.93:~
scp ca.pem kubernetes.pem kubernetes-key.pem ppp@192.168.0.152:~
scp ca.pem kubernetes.pem kubernetes-key.pem raspberrypi@192.168.0.151:~
scp ca.pem kubernetes.pem kubernetes-key.pem shsnail@192.168.0.154:~
scp ca.pem kubernetes.pem kubernetes-key.pem him@192.168.0.153:~
-	Prepare the 192.168.0.90, 192.168.0.91 machine
o	Get administrator privileges
$ sudo -s
o	Add the Docker repository key
# curl �fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
o	Add the Docker repository
# add-apt-repository \
"deb https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
$(lsb_release -cs) \
stable"
o	Update the list of packages
# apt-get update
o	Install Docker 17.03
# apt-get install -y docker-ce=$(apt-cache madison docker-ce | grep 17.03 | head -1 | awk '{print $3}')
o	Add the Google repository key.
# curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
o	Add the Google repository.
# nano /etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io kubernetes-xenial main
o	Update the list of packages.
# apt-get update
o	Install kubelet, kubeadm and kubectl.
# apt-get install kubelet kubeadm kubectl
o	Disable the swap.
# swapoff �a
# sed -i '/ swap / s/^/#/' /etc/fstab
-	Installing and configuring Etcd on the 192.168.0.90 machine
o	Create a configuration directory for Etcd.
$ sudo mkdir /etc/etcd /var/lib/etcd
o	Move the certificates to the configuration directory.
$ sudo mv ~/ca.pem ~/kubernetes.pem ~/kubernetes-key.pem /etc/etcd
o	Download the etcd binaries.
$ wget https://github.com/coreos/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz
o	Extract the etcd archive.
$ tar xvzf etcd-v3.3.9-linux-amd64.tar.gz
o	Move the etcd binaries to /usr/local/bin.
$ sudo mv etcd-v3.3.9-linux-amd64/etcd* /usr/local/bin/
o	Create an etcd systemd unit file.
$ sudo vim /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos


[Service]
ExecStart=/usr/local/bin/etcd \
--name 192.168.0.90 \
--cert-file=/etc/etcd/kubernetes.pem \
--key-file=/etc/etcd/kubernetes-key.pem \
--peer-cert-file=/etc/etcd/kubernetes.pem \
--peer-key-file=/etc/etcd/kubernetes-key.pem \
--trusted-ca-file=/etc/etcd/ca.pem \
--peer-trusted-ca-file=/etc/etcd/ca.pem \
--peer-client-cert-auth \
--client-cert-auth \
--initial-advertise-peer-urls https://192.168.0.90:2380 \
--listen-peer-urls https://192.168.0.90:2380 \
--listen-client-urls https://192.168.0.90:2379,http://127.0.0.1:2379 \
--advertise-client-urls https://192.168.0.90:2379 \
--initial-cluster-token etcd-cluster-0 \
--initial-cluster 192.168.0.90=https://192.168.0.90:2380,192.168.0.91=https://192.168.0.91:2380 \
--initial-cluster-state new \
 --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
o	Reload the daemon configuration.
$ sudo systemctl daemon-reload
o	Enable etcd to start at boot time.
$ sudo systemctl enable etcd
o	Start etcd.
$ sudo systemctl start etcd
-	Installing and configuring Etcd on the 192.168.0.91 machine
o	Create a configuration directory for Etcd.
$ sudo mkdir /etc/etcd /var/lib/etcd
o	Move the certificates to the configuration directory.
$ sudo mv ~/ca.pem ~/kubernetes.pem ~/kubernetes-key.pem /etc/etcd
o	Download the etcd binaries.
$ wget https://github.com/coreos/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz
o	Extract the etcd archive.
$ tar xvzf etcd-v3.3.9-linux-amd64.tar.gz
o	Move the etcd binaries to /usr/local/bin.
$ sudo mv etcd-v3.3.9-linux-amd64/etcd* /usr/local/bin/
o	Create an etcd systemd unit file.
$ sudo vim /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/local/bin/etcd \
--name 192.168.0.91 \
--cert-file=/etc/etcd/kubernetes.pem \
--key-file=/etc/etcd/kubernetes-key.pem \
--peer-cert-file=/etc/etcd/kubernetes.pem \
--peer-key-file=/etc/etcd/kubernetes-key.pem \
--trusted-ca-file=/etc/etcd/ca.pem \
--peer-trusted-ca-file=/etc/etcd/ca.pem \
--peer-client-cert-auth \
--client-cert-auth \
--initial-advertise-peer-urls https://192.168.0.91:2380 \
--listen-peer-urls https://192.168.0.91:2380 \
--listen-client-urls https://192.168.0.91:2379,http://127.0.0.1:2379 \
--advertise-client-urls https://192.168.0.91:2379 \
--initial-cluster-token etcd-cluster-0 \
--initial-cluster 192.168.0.90=https://192.168.0.90:2380,192.168.0.91=https://192.168.0.91:2380\
--initial-cluster-state new \
--data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
o	Reload the daemon configuration.
$ sudo systemctl daemon-reload
o	Enable etcd to start at boot time.
$ sudo systemctl enable etcd
o	Start etcd.
$ sudo systemctl start etcd
o	Verify that the cluster is up and running.
$ ETCDCTL_API=3 etcdctl member list
-	Initializing the 192.168.0.90 master node
o	Create the configuration file for kubeadm.
$ nano config.yaml
apiVersion: kubeadm.k8s.io/v1beta1
kind: MasterConfiguration
tokenTTL: �0�
---
apiVersion: kubeadm.k8s.io/v1beta1
kind: ClusterConfiguration
kubernetesVersion: stable
apiServer:
certSANs:
- 192.168.0.93
controlPlaneEndpoint: "192.168.0.93:6443"
etcd:
external:
endpoints:
- https://192.168.0.90:2379
- https://192.168.0.91:2379
caFile: /etc/etcd/ca.pem
certFile: /etc/etcd/kubernetes.pem
keyFile: /etc/etcd/kubernetes-key.pem
o	Initialize the machine as a master node.
$ sudo kubeadm init --config=config.yaml
o	Copy the certificates to the other masters.
$ sudo scp -r /etc/kubernetes/pki sguyennet@192.168.0.91:~
o	Setup kubeconfig
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

-	Initializing the 192.168.0.91 master node
o	Remove the apiserver.crt and apiserver.key.
$ rm ~/pki/apiserver.*
o	Move the certificates to the /etc/kubernetes directory.
$ sudo mv ~/pki /etc/kubernetes/
o	Create the configuration file for kubeadm.
$ nano config.yaml
apiVersion: kubeadm.k8s.io/v1beta1
kind: MasterConfiguration
tokenTTL: �0�
---
apiVersion: kubeadm.k8s.io/v1beta1
kind: ClusterConfiguration
kubernetesVersion: stable
apiServer:
certSANs:
- 192.168.0.93
controlPlaneEndpoint: "192.168.0.93:6443"
etcd:
external:
endpoints:
- https://192.168.0.90:2379
- https://192.168.0.91:2379
caFile: /etc/etcd/ca.pem
certFile: /etc/etcd/kubernetes.pem
keyFile: /etc/etcd/kubernetes-key.pem
o	Initialize the machine as a master node.
$ sudo kubeadm init --config=config.yaml	
o	Setup kubeconfig
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

-	Initializing the worker nodes
o	Execute the "kubeadm join" command that you copied from the last step of the initialization of the masters on each worker node.
$ sudo kubeadm join 192.168.0.93:6443 --token 1g1njb.ul5eu1b9h3ebqp3g \
    --discovery-token-ca-cert-hash sha256:250d64c06ed2c8f71c895441d5facb560abba1abc8904c537d8006c8fb8d3337
-	Configuring kubectl on the client machine(Loadbalancer)
o	SSH to one of the master node.
$ ssh sguyennet@192.168.0.90
o	Add permissions to the admin.conf file.
$ sudo chmod +r /etc/kubernetes/admin.conf
o	From the client machine, copy the configuration file.
$ scp sguyennet@192.168.0.90:/etc/kubernetes/admin.conf .
o	Create the kubectl configuration directory.
$ mkdir ~/.kube
o	Move the configuration file to the configuration directory.
$ mv admin.conf ~/.kube/config
o	Modify the permissions of the configuration file.
$ chmod 600 ~/.kube/config
o	Go back to the SSH session on the master and change back the permissions of the configuration file.
$ sudo chmod 600 /etc/kubernetes/admin.conf
o	check that you can access the Kubernetes API from the client machine.
$ kubectl get nodes
-	Deploying the overlay network
o	Install weave-net network driver on master node
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
7.	On the master and all the workers run the following command
$ sudo sysctl net.bridge.bridge-nf-call-iptables=1




Microservice Kubernetes Sample
=====================

[Deutsche Anleitung zum Starten des Beispiels](WIE-LAUFEN.md)

This sample is like the sample for my Microservices Book
 ([English](http://microservices-book.com/) /
 [German](http://microservices-buch.de/)) that you can find at
 https://github.com/ewolff/microservice .

However, this demo uses [Kubernetes](https://kubernetes.io/) as Docker
environment. Kubernetes also support service discovery and load
balancing. An Apache httpd as a reverse proxy routes the calls to the
services.

This project creates a complete micro service demo system in Docker
containers. The services are implemented in Java using Spring and
Spring Cloud.



It uses three microservices:
- `Order` to process orders.
- `Customer` to handle customer data.
- `Catalog` to handle the items in the catalog.

How to run
---------

See [How to run](HOW-TO-RUN.md).


Apache HTTP Load Balancer
------------------------

Apache HTTP is used to provide the web page of the demo at
port 8080. It also forwards HTTP requests to the microservices. This
is not really necessary as each service has its own port on the
Minikube host but it provides a single point of entry for the whole system.
Apache HTTP is configured as a reverse proxy for this.
Load balancing is left to Kubernetes.

To configure this Apache HTTP needs to get all registered services from
Kubernetes. It just uses DNS for that.

Please refer to the subdirectory [microservice-kubernetes-demo/apache](microservice-kubernetes-demo/apache/) to see how this works.


Remarks on the Code
-------------------

The microservices are:

- [microservice-kubernetes-demo-catalog](microservice-kubernetes-demo/microservice-kubernetes-demo-catalog) is the application to take care of items.
- [microservice-kubernetes-demo-customer](microservice-kubernetes-demo/microservice-kubernetes-demo-customer) is responsible for customers.
- [microservice-kubernetes-demo-order](microservice-kubernetes-demo/microservice-kubernetes-demo-order) does order processing. It uses
  microservice-kubernetes-demo-catalog and microservice-kubernetes-demo-customer.

The microservices use REST to communicate to each other.
See e.g. [CatalogClient](microservice-kubernetes-demo/microservice-kubernetes-demo-order/src/main/java/com/ewolff/microservice/order/clients/CatalogClient.java) .
The hostname is configurable to allow tests with stubs.
The default is `catalog` which works with Kubernetes.
Other microservices are found using Kubernetes built-in DNS.
Kubernetes does the load balancing on the IP level.

The microservices have a Java main application in `src/test/java` to
run them stand alone. `microservice-demo-order` uses a stub for the
other services then. Also there are tests that use _consumer-driven
contracts_. That is why it is ensured that the services provide the
correct interface. These CDC tests are used in microservice-demo-order
to verify the stubs. In `microservice-kubernetes-demo-customer` and
`microserivce-kubernetes-demo-catalog` they are used to verify the implemented
REST services.

Note that the code has no dependencies on Kubernetes.


-----------------------------------------------------------
Credits to https://github.com/ewolff/microservice-kubernetes